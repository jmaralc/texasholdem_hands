import pytest

from texasholdem_hands.card import Card


class TestCard:

    def test_should_be_possible_to_be_generated_from_a_string(self):
        # Given
        card_string = "A♦"

        # When

        mycard = Card(card_string)

        # Then
        assert isinstance(mycard, Card)

    def test_should_return_a_card_with_a_rank_and_suit_based_on_a_two_character_string(self):
        # Given
        card_string = "A♦"

        # When

        mycard = Card(card_string)

        # Then
        assert mycard.rank == card_string[0]
        assert mycard.suit == card_string[1]

    def test_should_return_exception_error_if_string_has_less_than_2_characters(self):
        # Given
        card_string = "A"

        # When
        # Then
        with pytest.raises(AttributeError):
            Card(card_string)

    def test_should_return_exception_error_if_string_has_more_than_3_characters(self):
        # Given
        card_string = "A♦A♦"

        # When
        # Then
        with pytest.raises(AttributeError):
            Card(card_string)
    def test_should_return_exception_error_if_string_has_3_characters_and_rank_is_numeric_but_not_10(self):
        # Given
        card_string = "111A"

        # When
        # Then
        with pytest.raises(AttributeError):
            Card(card_string)

    def test_should_return_exception_error_if_string_has_3_characters_and_rank_is_numeric_but_not_10(self):
        # Given
        card_string = "111A"

        # When
        # Then
        with pytest.raises(AttributeError):
            Card(card_string)


    def test_should_return_a_card_with_a_rank_and_suit_based_on_a_three_character_string(self):
        # Given
        card_string = "10♦"

        # When

        mycard = Card(card_string)

        # Then
        assert mycard.rank == "10"
        assert mycard.suit == "♦"

    def test_two_cards_should_be_comparable_such_two_cards_with_same_rank_are_equal(self):
        # Given
        card1 = Card("K♦")
        card2 = Card("K♥")

        # When
        # Then
        assert card1 == card2

    def test_two_cards_should_be_comparable_such_a_king_is_larger_than_a_10(self):
        # Given
        card1 = Card("K♦")
        card2 = Card("10♥")

        # When
        # Then
        assert card1 > card2

    def test_two_cards_should_be_comparable_such_a_king_is_less_than_a_ace(self):
        # Given
        card1 = Card("K♦")
        card2 = Card("A♥")

        # When
        # Then
        assert card1 < card2

    def test_a_collection_of_cards_should_be_sortable(self):
        # Given
        card_collection = [
            Card("K♦"),
            Card("A♥"),
            Card("J♣"),
            Card("9♠"),
            Card("3♠")
        ]

        expected_result = [
            Card("3♠"),
            Card("9♠"),
            Card("J♣"),
            Card("K♦"),
            Card("A♥"),
        ]
        # When
        # Then
        assert sorted(card_collection) == expected_result

    def test_should_have_a_representation_similar_to_the_string_is_comming_from(self):
        # Given
        card_string = "10♦"

        # When

        mycard = Card(card_string)

        # Then
        assert mycard.__repr__() == card_string

    def test_should_have_be_able_to_provide_its_rank_weight(self):
        # Given
        card_string = "A♦"

        # When

        mycard = Card(card_string)

        # Then
        assert mycard.get_rank_weight() == 14
