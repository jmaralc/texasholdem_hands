from texasholdem_hands.hand import hand


def test_hand_should_return_a_pair():
    # Given
    expected_result = {"type": "pair", "rank": ["A", "J", "10", "5"]}
    hole_cards = ["A♠", "A♦"]
    community_cards = ["J♣", "5♥", "10♥", "2♥", "3♦"]

    # When
    result = hand(hole_cards, community_cards)

    # Then
    assert result == expected_result

def test_hand_should_return_a_nothing():
    # Given
    expected_result = {"type": "nothing", "rank": ["A", "K", "J", "10", "5"]}
    hole_cards = ["A♠", "K♦"]
    community_cards = ["J♣", "5♥", "10♥", "2♥", "3♦"]

    # When
    result = hand(hole_cards, community_cards)

    # Then
    assert result == expected_result


def test_hand_should_return_a_two_pair():
    # Given
    expected_result = {"type": "two pair", "rank": ["A", "10", "K"]}
    hole_cards = ["A♠", "K♦"]
    community_cards = ["J♣", "A♥", "10♥", "2♥", "10♦"]

    # When
    result = hand(hole_cards, community_cards)

    # Then
    assert result == expected_result

def test_hand_should_return_a_three_of_a_kind():
    # Given
    expected_result = {"type": "three-of-a-kind", "rank": ["A", "K", "J"]}
    hole_cards = ["A♠", "K♦"]
    community_cards = ["J♣", "A♥", "10♥", "2♥", "A♦"]

    # When
    result = hand(hole_cards, community_cards)

    # Then
    assert result == expected_result


def test_hand_should_return_a_straight():
    # Given
    expected_result = {"type": "straight", "rank": ["K", "Q", "J", "10", "9"]}
    hole_cards = ["3♠", "K♦"]
    community_cards = ["Q♣", "J♥", "10♥", "9♥", "2♦"]

    # When
    result = hand(hole_cards, community_cards)

    # Then
    assert result == expected_result

def test_hand_should_return_a_second_straight():
    # Given
    expected_result = {"type": "straight", "rank": [ "Q", "J", "10", "9", "8"]}
    hole_cards = ["A♠", "A♦"]
    community_cards = ["Q♣", "J♥", "10♥", "9♥", "8♦"]

    # When
    result = hand(hole_cards, community_cards)

    # Then
    assert result == expected_result

def test_hand_should_return_a_flush():
    # Given
    expected_result = {"type": "flush", "rank": ["A", "Q", "J", "9", "2"]}
    hole_cards = ["A♥", "7♦"]
    community_cards = ["Q♥", "J♥", "2♥", "9♥", "8♦"]

    # When
    result = hand(hole_cards, community_cards)

    # Then
    assert result == expected_result


def test_hand_should_return_a_full_house():
    # Given
    expected_result = {"type": "full house", "rank": ["A", "J", "Q", "7"]}
    hole_cards = ["A♥", "7♦"]
    community_cards = ["Q♥", "J♥", "A♣", "J♣", "A♦"]

    # When
    result = hand(hole_cards, community_cards)

    # Then
    assert result == expected_result


def test_hand_should_return_a_four_of_a_kind():
    # Given
    expected_result = {"type": "four-of-a-kind", "rank": ["A", "J", "7"]}
    hole_cards = ["A♥", "7♦"]
    community_cards = ["A♠", "J♥", "A♣", "J♣", "A♦"]

    # When
    result = hand(hole_cards, community_cards)

    # Then
    assert result == expected_result


def test_hand_should_return_a_straight_flush():
    # Given
    expected_result = {"type": "straight-flush", "rank": ["K", "Q", "J", "10", "9"]}
    hole_cards = ["3♠", "K♥"]
    community_cards = ["Q♥", "J♥", "10♥", "9♥", "2♦"]

    # When
    result = hand(hole_cards, community_cards)

    # Then
    assert result == expected_result
