from __future__ import annotations


class Card:

    def __init__(self, card_string: str) -> None:

        if len(card_string) < 2:
            raise AttributeError("Card value too short!!")
        if len(card_string) > 3:
            raise AttributeError("Card value too long!!")
        if len(card_string) == 3:
            if card_string[:2] != "10":
                raise AttributeError("Card value too long!!")
            rank = card_string[:2]
        else:
            rank = card_string[0]

        self.rank = rank
        self.suit = card_string[-1]

    def __eq__(self, other: Card):
        return self.get_rank_weight() == other.get_rank_weight()

    def __gt__(self, other: Card):
        return self.get_rank_weight() > other.get_rank_weight()

    def __lt__(self, other: Card):
        return self.get_rank_weight() < other.get_rank_weight()

    def __repr__(self):
        return f"{self.rank}{self.suit}"

    def get_rank_weight(self):
        match self.rank:
            case "A":
                return 14
            case "K":
                return 13
            case "Q":
                return 12
            case "J":
                return 11
            case "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" | "10":
                return int(self.rank)
