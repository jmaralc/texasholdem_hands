from texasholdem_hands.hands.pairs import Pairs
from texasholdem_hands.hands.triplets import Triplets
from texasholdem_hands.hands.straights import Straights
from texasholdem_hands.hands.flushs import Flushs
from texasholdem_hands.hands.full_houses import FullHouses
from texasholdem_hands.hands.fours import Fours
from texasholdem_hands.hands.straight_flushs import StraightFlushs
from texasholdem_hands.card import Card


hands = [
    Pairs,
    Triplets,
    Straights,
    Flushs,
    FullHouses,
    Fours,
    StraightFlushs,
]


def hand(hole_cards: list, community_cards: list) -> dict[str, str | list[str]]:
    total_cards = [Card(c) for c in hole_cards + community_cards]

    hands_results = [h().check(cards=total_cards) for h in hands]

    result = sorted(hands_results, key=lambda h: h.get("weight")).pop()
    del result["weight"]
    return result
