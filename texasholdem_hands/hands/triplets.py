from itertools import groupby

from texasholdem_hands.hands.hand import Hand


class Triplets(Hand):

    def check(self, cards: list) -> dict[str, str | list[str]]:

        triplets = []
        for k, same_rank_cards in groupby(sorted(cards, reverse=True), lambda c: c.rank):
            card_group = list(same_rank_cards)
            if len(card_group) == 3:
                triplets.append(card_group)

        amount_of_triplets = len(triplets)
        if amount_of_triplets == 1:
            triplet_rank = [t[0].rank for t in triplets]
            rest_cards = sorted(list(filter(lambda c: c.rank not in triplet_rank, cards)), reverse=True)
            rest_cards_ranks = [c.rank for c in rest_cards]

            return {
                "type": "three-of-a-kind",
                "rank": triplet_rank + rest_cards_ranks[:self.amount_to_return - (3 * len(triplet_rank))],
                "weight": 3
            }
        return self.nothing_in_the_hand(cards)
