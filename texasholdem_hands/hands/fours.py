from itertools import groupby

from texasholdem_hands.hands.hand import Hand


class Fours(Hand):

    def check(self, cards: list) -> dict[str, str | list[str]]:

        pokers = []
        for k, same_rank_cards in groupby(sorted(cards, reverse=True), lambda c: c.rank):
            card_group = list(same_rank_cards)
            if len(card_group) == 4:
                pokers.append(card_group)

        if pokers:
            fours_rank = pokers.pop()[0].rank
            rest_cards = sorted(list(filter(lambda c: c.rank not in fours_rank, cards)), reverse=True)
            unique_rest_cards = []
            for card in rest_cards:
                if card not in unique_rest_cards:
                    unique_rest_cards.append(card)
            rest_cards_ranks = [c.rank for c in sorted(unique_rest_cards, reverse=True)]

            return {
                "type": "four-of-a-kind",
                "rank": [fours_rank] + rest_cards_ranks[:self.amount_to_return - 1],
                "weight": 8
            }
        return self.nothing_in_the_hand(cards)
