from itertools import groupby

from texasholdem_hands.hands.hand import Hand


class FullHouses(Hand):

    def check(self, cards: list) -> dict[str, str | list[str]]:

        triplets = []
        for k, same_rank_cards in groupby(sorted(cards, reverse=True), lambda c: c.rank):
            card_group = list(same_rank_cards)
            if len(card_group) == 3:
                triplets.append(card_group)

        amount_of_triplets = len(triplets)
        if amount_of_triplets == 1:
            triplet_rank = [t[0].rank for t in triplets]
            rest_cards = sorted(list(filter(lambda c: c.rank not in triplet_rank, cards)), reverse=True)

            pairs = []
            for k, same_rank_cards in groupby(sorted(rest_cards, reverse=True), lambda c: c.rank):
                card_group = list(same_rank_cards)
                if len(card_group) == 2:
                    pairs.append(card_group)

            amount_of_pairs = len(pairs)
            if amount_of_pairs == 1:
                pair_rank = [p[0].rank for p in pairs]
                rest_cards = sorted(list(filter(lambda c: c.rank not in pair_rank, rest_cards)), reverse=True)
                rest_cards_ranks = [c.rank for c in rest_cards]

                return {
                    "type": "full house",
                    "rank": triplet_rank + pair_rank + rest_cards_ranks[:self.amount_to_return - (2 * 1 - 3 * 1)],
                    "weight": 7
                }
        return self.nothing_in_the_hand(cards)