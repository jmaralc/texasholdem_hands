from itertools import groupby

from texasholdem_hands.hands.hand import Hand


class Flushs(Hand):

    def check(self, cards: list) -> dict[str, str | list[str]]:

        flush = []
        for k, same_rank_cards in groupby(sorted(cards, reverse=True, key= lambda card: card.suit), lambda c: c.suit):
            card_group = list(same_rank_cards)
            if len(card_group) == 5:
                flush.append(card_group)

        if len(flush) == 1:
            flush_rank = [card.rank for card in sorted(flush.pop(), reverse=True)]

            return {
                "type": "flush",
                "rank": flush_rank,
                "weight": 6
            }
        return self.nothing_in_the_hand(cards)