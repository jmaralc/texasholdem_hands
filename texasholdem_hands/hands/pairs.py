from itertools import groupby

from texasholdem_hands.hands.hand import Hand


class Pairs(Hand):

    pairs_types = {
        1: "pair",
        2: "two pair"
    }

    def check(self, cards: list) -> dict[str, str | list[str]]:

        pairs = []
        for k, same_rank_cards in groupby(sorted(cards, reverse=True), lambda c: c.rank):
            card_group = list(same_rank_cards)
            if len(card_group) == 2:
                pairs.append(card_group)

        amount_of_pairs = len(pairs)
        if amount_of_pairs >= 1:
            pair_rank = [p[0].rank for p in pairs]
            rest_cards = sorted(list(filter(lambda c: c.rank not in pair_rank, cards)), reverse=True)
            rest_cards_ranks = [c.rank for c in rest_cards]

            return {
                "type": self.pairs_types.get(amount_of_pairs),
                "rank": pair_rank + rest_cards_ranks[:self.amount_to_return - (2 * len(pair_rank))],
                "weight": len(pair_rank)
            }
        return self.nothing_in_the_hand(cards)
