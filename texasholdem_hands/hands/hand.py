from abc import ABC, abstractmethod


class Hand(ABC):

    amount_to_return = 5

    @abstractmethod
    def check(self, cards: list) -> dict[str, str | list[str]]:
        raise NotImplementedError("Not yet implemented")

    def nothing_in_the_hand(self, cards: list) -> dict[str, str | list[str]]:
        rest_cards = sorted(cards, reverse=True)
        rest_cards_ranks = [c.rank for c in rest_cards]
        return {
            "type": "nothing",
            "rank": rest_cards_ranks[:self.amount_to_return],
            "weight": 0
        }