from texasholdem_hands.card import Card
from texasholdem_hands.hands.hand import Hand


class Straights(Hand):

    straight_amount = 5

    def check(self, cards: list) -> dict[str, str | list[str]]:

        unique_cards = []
        for card in cards:
            if card not in unique_cards:
                unique_cards.append(card)

        sorted_cards = sorted(unique_cards, reverse=True)

        if len(sorted_cards) < 5:
            return self.nothing_in_the_hand(cards)

        candidate = []
        for index in range(len(sorted_cards)+1-self.straight_amount):
            candidate.append(sorted_cards[index])
            for rest_card in sorted_cards[index+1:]:
                if self.__consecutives_cards(candidate[-1], rest_card):
                    candidate.append(rest_card)
            if len(candidate) >= self.straight_amount:
                break
            else:
                candidate = []

        if len(candidate) >= self.straight_amount:
            straight_rank = [card.rank for card in candidate[:self.straight_amount]]

            return {
                "type": "straight",
                "rank": straight_rank,
                "weight": 5
            }
        return self.nothing_in_the_hand(cards)

    def __consecutives_cards(self, card1: Card, card2: Card):
        return card2.get_rank_weight() == (card1.get_rank_weight() - 1)
